rm(list = ls())
library(ggplot2)
library(MASS)

##############
# EXERCICE 1 #
##############

# Paramètres 

n <- 100
m <- 1000
theta0 <- 1

graine <- 123

# Estimateur sans biais ?

set.seed(seed = graine)
X <- rexp(n = n*m,rate = 1/theta0)
X <- matrix(data = X, ncol = m, nrow = n)
estimateur <- colMeans(X)

Biais <- mean(estimateur)-theta0
if (Biais < 1e-6){
  cat("L'estimateur est sans biais pour theta0 =", theta0, "avec B =",Biais,"\n")
} else {
  cat("L'estimateur est biaisé pour theta0 =", theta0, "avec B =",Biais,"\n")
}

# Estimateur asymptotiquement sans biais ?

set.seed(seed = graine)
X <- rexp(n = n*m,rate = 1/theta0)
X <- matrix(data = X, ncol = m, nrow = n)
S <- apply(X = X, MARGIN = 2, FUN = cumsum)
N <- matrix(rep(x = 1:n, times = m), ncol = m, nrow = n)
Estimateur <- rowMeans(S/N)
Biais <- Estimateur - theta0

Biais <- as.data.frame(Biais)
ggplot(Biais, aes(x = 1:n, y = Biais)) +
  geom_line() +
  xlab("Taille de l'échantillon") +
  ylab("Biais empirique") + 
  geom_hline(yintercept = 0, col = "red")

# Estimateur convergent ?

jstar <- 1
nset <- 1:n
X <-  rexp(n = n*m,rate = 1/theta0)
Estimateur <- cumsum(X)/nset

Estimateur <- Estimateur[jstar:n]
Estimateur <- as.data.frame(Estimateur)  
ggplot(Estimateur, aes(x = jstar:n, y = Estimateur)) +
  geom_line() +
  xlab("Taille de l'échantillon") +
  ylab("Estimateur") +
  geom_hline(yintercept = theta0, col = "red")

# Estimateur asymptotiquement normal ?

set.seed(seed = graine)
X <- rexp(n = n*m,rate = 1/theta0)
X <- matrix(data = X, ncol = m, nrow = n)
Estimateur <- colMeans(X)
Estimateur.CR <- (Estimateur - mean(Estimateur))/sd(Estimateur)

Estimateur.CR <- as.data.frame(Estimateur.CR)  
ggplot(Estimateur.CR, aes(x = Estimateur.CR)) + 
  geom_histogram(aes(y=..density..), bins = nclass.FD(Estimateur.CR$Estimateur.CR)) +
  stat_function(fun = dnorm, col = "red") +
  xlab("") +
  ylab("") 

##############
# EXERCICE 2 #
##############

# Paramètres 

n <- 50
m <- 100

alpha <- 2
beta <- 2

graine <- 123

# Estimateur sans biais ?

set.seed(seed = graine)
X <- rweibull(n = n*m, shape = beta, scale = alpha)
X <- matrix(data = X, ncol = m, nrow = n)
Estimateur <- apply(X = X, MARGIN = 2, FUN = fitdistr, densfun = "weibull")
Estimateur <- matrix(data = unlist(Estimateur), ncol = 10, byrow = TRUE)
Estimateur <- Estimateur[,1:2]

Biais <- mean(Estimateur) - c(alpha, beta)
if (Biais[1] < 1e-6){
  cat("L'estimateur est sans biais pour alpha =", alpha, "avec B =",Biais[1],"\n")
} else {
  cat("L'estimateur est biaisé pour alpha =", alpha, "avec B =",Biais[1],"\n")
}
if (Biais[2] < 1e-6){
  cat("L'estimateur est sans biais pour beta =", beta, "avec B =",Biais[2],"\n")
} else {
  cat("L'estimateur est biaisé pour beta =", beta, "avec B =",Biais[2],"\n")
}

# Estimateur asymptotiquement sans biais ?

set.seed(seed = graine)
X <- rweibull(n = n*m, shape = beta, scale = alpha)
X <- matrix(data = X, ncol = m, nrow = n)

u <- 0.05
n0 <- floor(u*n)
n0 <- ifelse(test = n0<2, yes = 2, no = n0)
X2 <- sapply(1:(n-n0), function(x) { X[1:(n0+x),] })

Biais <- matrix(ncol = 2, nrow = (n-n0))

for (i in 1:(n-n0)) {
  res <- apply(X = X2[[i]], MARGIN = 1, FUN = fitdistr, densfun = "weibull")
  res <- matrix(data = unlist(res), ncol = 10, byrow = TRUE)
  res <- res[,1:2]
  Biais[i,] <- colMeans(res) -c(alpha, beta)
}

Biais <- as.data.frame(Biais)  
ggplot(Biais, aes(x = 1:(n-n0), y = Biais[,1])) +
  geom_line() +
  xlab("Taille de l'échantillon") +
  ylab("Biais") +
  geom_hline(yintercept = 0, col = "red")
ggplot(Biais, aes(x = 1:(n-n0), y = Biais[,2])) +
  geom_line() +
  xlab("Taille de l'échantillon") +
  ylab("Biais") +
  geom_hline(yintercept = 0, col = "red")


# Estimateur convergent ?

n <- 1000

set.seed(seed = graine)
X <- rweibull(n = n, shape = beta, scale = alpha)
u <- 0.05
n0 <- floor(u*n)
n0 <- ifelse(test = n0<2, yes = 2, no = n0)
alpha.est <- vector("numeric",n-n0)
beta.est <- vector("numeric",n-n0)

X2 <- sapply(1:(n-n0), function(x) { X[1:(n0+x)] }) 
Estimateur <- lapply(X = X2, FUN = fitdistr, densfun = "weibull")
Estimateur <- matrix(data = unlist(Estimateur), ncol = 10, byrow = TRUE)
Estimateur <- Estimateur[,1:2]

Estimateur <- as.data.frame(Estimateur)  
ggplot(Estimateur, aes(x = (n0+1):n, y = Estimateur[,1])) +
  geom_line() +
  xlab("Taille de l'échantillon") +
  ylab("Estimateur") +
  geom_hline(yintercept = alpha, col = "red")

ggplot(Estimateur, aes(x = (n0+1):n, y = Estimateur[,2])) +
  geom_line() +
  xlab("Taille de l'échantillon") +
  ylab("Estimateur") +
  geom_hline(yintercept = beta, col = "red")

# Estimateur asymptotiquement normal ?

set.seed(seed = graine)
X <- rweibull(n = n*m, shape = beta, scale = alpha)
X <- matrix(data = X, ncol = m, nrow = n)
Estimateur <- apply(X = X, MARGIN = 2, FUN = fitdistr, densfun = "weibull")
Estimateur <- matrix(data = unlist(Estimateur), ncol = 10, byrow = TRUE)
Estimateur <- Estimateur[,1:2]

Estimateur.CR <- Estimateur
Estimateur.CR[, 1] <- (Estimateur.CR[, 1] - mean(Estimateur.CR[, 1]))/sd(Estimateur.CR[, 1])
Estimateur.CR[, 2] <- (Estimateur.CR[, 2] - mean(Estimateur.CR[, 2]))/sd(Estimateur.CR[, 2])

Estimateur.CR <- as.data.frame(Estimateur.CR)  
ggplot(Estimateur.CR, aes(x = Estimateur.CR[,1])) + 
  geom_histogram(aes(y=..density..), bins = nclass.FD(Estimateur.CR[,1])) +
  stat_function(fun = dnorm, col = "red") +
  xlab("") +
  ylab("") 

Estimateur.CR <- as.data.frame(Estimateur.CR)  
ggplot(Estimateur.CR, aes(x = Estimateur.CR[,2])) + 
  geom_histogram(aes(y=..density..), bins = nclass.FD(Estimateur.CR[,2])) +
  stat_function(fun = dnorm, col = "red") +
  xlab("") +
  ylab("") 

n <- 100
m <- 100
a <- 1

##############
# EXERCICE 3 #
##############

# Question 4

test.normalite <- function(n,m,a) {
  a.est <- vector("numeric",m)
  for (i in 1:m) {
    U <- runif(n,0,a)
    a.est[i] <- max(U)
  }
  Z <<- scale(a.est)*sqrt(n) 
  plot(ecdf(Z),do.points=FALSE,lwd=3)
  x <- seq(from=min(Z),by=0.01,to=max(Z))
  y <- pnorm(x)
  lines(x,y,col="red",lwd=2)
}

test.normalite(n,m,a)

# Question 5

test.loi.limite <- function(n,m,a) {
  a.est <- vector("numeric",m)
  for (i in 1:m) {
    U <- runif(n,0,a)
    a.est[i] <- max(U)
  }
  Z <<- n*(1-a.est) 
  plot(ecdf(Z),do.points=FALSE,lwd=3)
  x <- seq(from=min(Z),by=0.01,to=max(Z))
  y <- pexp(x,rate=a)
  lines(x,y,col="red",lwd=2)
}

test.loi.limite(n,m,a)

