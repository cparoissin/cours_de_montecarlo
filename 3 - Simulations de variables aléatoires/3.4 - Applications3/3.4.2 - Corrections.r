#########################
#########################
# PREPARATION DU SCRIPT #
#########################
#########################

# On efface tous les objets
rm(list = ls())

# On charge les packages
library(ggplot2)

##################################################
##################################################
# FONCTIONS UTILES POUR LES DIFFERENTS EXERCICES #
##################################################
##################################################

# SIMULATION D'UNE CHAINE DE MARKOV

SimulMarkovChain <- function(nb.steps, P, nu) {
  check <- TRUE
  p <- ncol(P)
  check <- ((nrow(P)==p) & (length(nu)==p) & (sum(rowSums(P)==1)==p))
  if (check) {
    set.states <- (1:p)
    current.state <- sample(set.states,1,prob=nu)
    state <- current.state
    for (i in 1:nb.steps) {
      prb <- P[current.state,]
      current.state <- sample(set.states,1,prob=prb)
      state <- c(state,current.state)
    }
    return(state)
  } else {
    warning("Problem of dimension")
    return("NA")
  }
}

# Un exemple pour tester la fonction
nb.steps <- 20
P <- matrix(data = c(1/3,0,2/3,3/4,1/4,0,0,1/2,1/2), ncol = 3, byrow = TRUE)
nu <- c(1/2,1/4,1/4)
X <- SimulMarkovChain(nb.steps, P, nu)

MarkovC <- data.frame(0:nb.steps, X) 
colnames(MarkovC) <- c("time", "state")
ggplot() +
  geom_step(data = MarkovC, mapping=aes(x = time, y = state)) +
  geom_point(data = MarkovC, mapping=aes(x = time, y = state), color="red") + 
  xlab("Temps") +
  ylab("Etat")

# SIMULATION D'UN PROCESSUS DE POISSON - VERSION 1

SimulPoissonProcess.1 <- function(lambda, t.max) {
  n <- 0
  Sn <- 0
  tps <- 0
  while (Sn < t.max) {
    Xnew <- rexp(1,lambda)
    tps <- c(tps,Xnew)
    Sn <- Sn + Xnew
  }
  return(tps)
}

# Un exemple pour tester la fonction

lambda <- 1
t.max <- 10
X <- SimulPoissonProcess.1(lambda, t.max)

ProcPois <- data.frame(cumsum(X), 0:(length(X)-1)) 
colnames(ProcPois) <- c("time", "counter")
ggplot() +
  geom_step(data = ProcPois, mapping=aes(x = time, y = counter)) +
  geom_point(data = ProcPois, mapping=aes(x = time, y = counter), color="red") + 
  xlab("Temps") +
  ylab("Compteur")

# SIMULATION D'UN PROCESSUS DE POISSON - VERSION 2

SimulPoissonProcess.2 <- function(lambda, t.max) {
  n <- rpois(1,lambda*t.max)
  tps <- sort(runif(n,min=0,max=t.max))
  return(c(0,tps))
}

# Un exemple pour tester la fonction
lambda <- 1
t.max <- 10
X <- SimulPoissonProcess.2(lambda, t.max)

ProcPois <- data.frame(cumsum(X), 0:(length(X)-1)) 
colnames(ProcPois) <- c("time", "counter")
ggplot() +
  geom_step(data = ProcPois, mapping=aes(x = time, y = counter)) +
  geom_point(data = ProcPois, mapping=aes(x = time, y = counter), color="red") + 
  xlab("Temps") +
  ylab("Compteur")

# SIMULATION D'UN PROCESS MARKOVIEN DE SAUTS
SimulMarkovJumpProcess <- function(t.max, A, nu) {
  clock <- max(diag(abs(A)))
  P <- diag(c(1,1)) + A/clock
  tps <- SimulPoissonProcess.2(clock, t.max)
  X <- SimulMarkovChain(length(tps)-1, P, nu)
  X <- X-1
  return(cbind(tps,X))
}

##############
##############
# EXERCICE 1 #
##############
##############

# PARAMETRES

# Taux de transition
# 0 : état de marche
# 1 : état de panne
lambda <- 1
mu <- 2

# Instant auquel on estime la disponibilité
t <- 20

# Calcul de la disponibilité selon la formule explicite
availability <- lambda/(lambda+mu)*(1-exp(-lambda*t))

# Nombre de répétition
n <- 100
alpha <- 0.05
z <- qnorm(1-alpha/2)

# ESTIMATION DE LA DISPONIBILITE D'UN COMPOSANT 
# SIMULATION DIRECTE 

# On commence par simuler le modèle de manière directe en alternant des durées de loi exponentielle
# Avantages : simple à mettre en oeuvre, intuitif
# Inconvénient : trop spécifique à l'exercice, donc peu ré-utilisable pour d'autres modèles

SimulBinaryComponent.v1 <- function(t.max, lambda, mu) {
  # Etat initial du composant
  current.state <- 0
  # Initialisation du chronomètre
  current.time <- 0
  # Simulation tant que le chronomètre est inférieur à t.max
  while (current.time <= t.max) {
    # On simule l'instant du prochain événement
    tps <- (1-current.state)*rexp(n = 1, rate = lambda) + current.state*rexp(n = 1, rate = mu)
    # On change l'état
    current.state <- 1-current.state
    # On incrémente le compteur du chronomètre
    current.time <- current.time + tps
  }
  return(current.state)
}

# La fonction replicate permet de répéter plusieurs fois une fonction.
# Cela revient donc au même que de faire des boucles for
X <- replicate(n,SimulBinaryComponent.v1(t, lambda, mu))
estimation <- mean(X==0)

CI.lower <- estimation - z*sqrt(estimation*(1-estimation)/n)
CI.upper <- estimation + z*sqrt(estimation*(1-estimation)/n)

cat("*** METHODE DIRECTE ***\n")
cat("Disponibilité théorique du compososant à l'instant",t,":",availability,"\n")
cat("Estimation de la disponibilité",estimation,"\n")
cat("Intervalle de confiance pour la disponibilité : [",CI.lower,",",CI.upper,"]\n",sep="")

# SIMULATION D'UNE TRAJECTOIRE #
# Même algorithme mais on stocke les états successifs et les instants de saut

SimulBinaryComponent.v2 <- function(t.max, lambda, mu) {
  current.state <- 0
  current.time <- 0
  state <- current.state
  time <- current.time
  while (current.time <= t.max) {
    tps <- (1-current.state)*rexp(n = 1, rate = lambda) + current.state*rexp(n = 1, rate = mu) 
    current.state <- 1-current.state
    current.time <- current.time + tps
    state <- c(state,current.state)
    time <- c(time,current.time)
  }
  return(cbind(time,state))
}

X <- SimulBinaryComponent.v2(t, lambda, mu)

X <- as.data.frame(X) 
ggplot() +
  geom_step(data = X, mapping=aes(x = time, y = state)) +
  geom_point(data = X, mapping=aes(x = time, y = state), color="red") + 
  xlab("Temps") +
  ylab("Etat")

# ESTIMATION DE LA DISPONIBILITE D'UN COMPOSANT 
# SIMULATION INDIRECTE

# On commence par simuler le modèle de manière directe en alternant des durées de loi exponentielle
# Avantages : nécessite des fonctions qui pourront être ré-utilisées par ailleurs
# Inconvénient : plus complexe

A <- matrix(data = c(-lambda,lambda,mu,-mu), ncol = 2, byrow = TRUE)
nu <- c(1,0)

X <- replicate(n,tail(SimulMarkovJumpProcess(t.max, A, nu)[,2],1))
estimation <- mean(X==0)

CI.lower <- estimation - z*sqrt(estimation*(1-estimation)/n)
CI.upper <- estimation + z*sqrt(estimation*(1-estimation)/n)

cat("*** METHODE INDIRECTE ***\n")
cat("Disponibilité théorique du compososant à l'instant",t,":",availability,"\n")
cat("Estimation de la disponibilité",estimation,"\n")
cat("Intervalle de confiance pour la disponibilité : [",CI.lower,",",CI.upper,"]\n",sep="")

#---> BUG ???? 

# CALCULS PARALLELES 

# On va essayer d'accélerer les calculs en faisant du calcul parallèle et en exploitant au mieux votre machine
# Partie non demandée, c'est pour votre culture générale !!!

# Appel aux packages permettant de faire du calcul parallèle de manière très simple
library(doParallel)
library(foreach)
library(doRNG)

# Détection automatique des coeurs de votre machine et allocation des coeurs
NbCores <- detectCores()
cl <- makeCluster(NbCores)
registerDoParallel(cl)
getDoParWorkers()

# La fonction foreach permet de remplacer les boucles for et l'utilisation de la fonction replicate
X <- foreach(w = 1:n, .combine = c, .options.RNG = 123) %dorng% SimulBinaryComponent.v1(t, lambda, mu)
X = as.vector(X)

# Arrêt de l'utilisation des coeurs
stopCluster(cl)

# On récupère l'estimation 
estimation <- mean(X==0)
CI.lower <- estimation - z*sqrt(estimation*(1-estimation)/n)
CI.upper <- estimation + z*sqrt(estimation*(1-estimation)/n)

cat("*** METHODE DIRECTE AVEC CALCUL PARALLELE ***\n")
cat("Disponibilité théorique du compososant à l'instant",t,":",availability,"\n")
cat("Estimation de la disponibilité",estimation,"\n")
cat("Intervalle de confiance pour la disponibilité : [",CI.lower,",",CI.upper,"]\n",sep="")

##############
##############
# EXERCICE 2 #
##############
##############

# PARAMETRES

# Taux des lois exponentielles
lambda <- 1
mu <- .2

# Nombre de guichets/serveurs
s <- 5

# Instant auquel on estime la saturation
tmax <- 20

# Nombre de répétition
nbrep <- 10000
alpha <- 0.05
z <- qnorm(1-alpha/2)

# Pour le moment, correction uniquement avec la méthode directe

saturation <- vector(mode = "numeric", length = nbrep)

for (i in 1:nbrep) {
  t <- 0
  # nombre de personnes dans la file
  etat <- 0
  while (t < tmax) {
    duree1 <- rexp(n = 1, rate = lambda)
    if (etat!=0) {
      duree2 <- rexp(n = 1, rate = mu*min(etat,s))
    } else {
      duree2 <- Inf
    }
    duree <- min(duree1,duree2)
    t <- t + duree
    etat <- etat + (2*(duree==duree1)-1)
  }
  saturation[i] <- as.integer(etat>=s)
}

proba.saturation.est <- mean(saturation)
CI.lower <- proba.saturation.est-z*sqrt(proba.saturation.est*(1-proba.saturation.est)/nbrep)
CI.upper <- proba.saturation.est+z*sqrt(proba.saturation.est*(1-proba.saturation.est)/nbrep)

cat("Estimation de la probabilité de saturation de la file :",proba.saturation.est,"\n")
cat("Intervalle de confiance pour la probabilité de saturation de la file : [",CI.lower,",",CI.upper,"]\n",sep="")

##############
##############
# EXERCICE 3 #
##############
##############

# https://en.wikipedia.org/wiki/Ruin_theory 

# PARAMETRES

# Intensité du processus de Poisson
lambda <- .1
# Capital inital de la compagnie d'assurance 
x0 <- 20
# Taux d'interêt
c <- 0.1
c <- 0.5
c <- 0.8
# Montant des sinistes : loi exponentielle de paramètre mu
mu <- 0.5

# Horizon
tmax <- 50
h <- tmax

# Nombre de répétition
nbrep <- 10000
alpha <- 0.05
z <- qnorm(1-alpha/2)

# Fonction pour simuler l'évolution de la richesse de la compagnie
richesse <- function(x0, c, lambda, mu, h) {
  # on simule le processus de Poisson
  t <- SimulPoissonProcess.2(lambda = lambda, t.max = h)
  # selon la valeur des paramètres, on peut avoir aucun événement... ce qui est gênant
  while (length(t)==1) {
    t <- SimulPoissonProcess.2(lambda = lambda, t.max = h)
  }  
  n <- length(t)
  # on rajoute l'horizon h
  t[n+1] <- h
  # on calcule les durées entre deux événements consécutifs
  s <- diff(t)
  X <- vector(mode = "numeric", length = 2*n)
  X[1] <- x0
  cnt <- 1
  # à chaque événement...
  for (i in 1:(n-1)) { 
    # on calcule la richesse avant le sinistre...
    X[cnt+1] <- X[cnt] + c*s[i]
    # et la richesse après paiement des remboursements
    X[cnt+2] <- X[cnt+1]-rexp(1,mu)
    cnt <- cnt+2
  }
  X[cnt+1] <- X[cnt]+c*s[n]
  return(list(t,X))
}

# REPRESENTATION GRAPHIQUE D'UNE TRAJECTOIRE

SimRichesse <- richesse(x0, c, lambda, mu, h)
t <- SimRichesse[[1]]
X <- SimRichesse[[2]]
n <- length(t)

tps <- t[1]
for (i in 2:(n-1)) {
  tps <- c(tps, t[i], t[i])
}
tps <- c(tps, t[n])

Evol <- data.frame(tps, X)
ggplot(Evol, aes(x = tps, y = X)) +
  geom_line() +
  xlab("Temps") +
  ylab("Richesse")

# ESTIMATION DE LA PROBABILITE DE RUINE

ruine <- vector(mode = "logical", length = nbrep)
for (i in 1:nbrep) {
  SimRichesse <- richesse(x0, c, lambda, mu, h)
  X <- SimRichesse[[2]]
  ruine[i] <- (sum(X<0)>0)
}

prob.ruine <- mean(ruine)
CI.lower <- prob.ruine - z*sqrt(prob.ruine*(1-prob.ruine)/nbrep)
CI.upper <- prob.ruine + z*sqrt(prob.ruine*(1-prob.ruine)/nbrep)

cat("Estimation de la probabilité de saturation de la file :",prob.ruine,"\n")
cat("Intervalle de confiance pour la probabilité de saturation de la file : [",CI.lower,",",CI.upper,"]\n",sep="")

