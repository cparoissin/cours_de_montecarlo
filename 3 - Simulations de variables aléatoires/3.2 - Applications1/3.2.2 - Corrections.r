#########################
# PREPARATION DU SCRIPT #
#########################

# On efface tous les objets
rm(list = ls())

# On charge les packages
library(ggplot2)

##############
# EXERCICE 1 #
##############

#--- LOI DES GRANDS NOMBRES ---#

LGN <- function(taille, rloi, moyenne = NA, titre = "") {
  # Taille de l'échantillon
  n <- taille
  # POint de départ pour la représentation graphique
  jstar <- 1
  # Taille successive des sous-échantillons
  nset <- 1:n
  # Simulation des variables aléatoires
  X <- rloi(n = n)
  # Moyennes empiriques successives sur les i premières variables aléatoires
  Xbar <- cumsum(X)/nset
  # On retire le début
  Xbar <- Xbar[jstar:n]
  # Représentation graphique
  Xbar <- as.data.frame(Xbar)  
  Graphique <- ggplot(Xbar, aes(x = jstar:n, y = Xbar)) +
    geom_line() +
    xlab("Taille de l'échantillon") +
    ylab("Moyenne empirique")
  if (!is.na(moyenne)) {
    Graphique <- Graphique + geom_hline(yintercept = moyenne, col = "red")
  }
  Graphique +
    labs(title = titre)
}

# LOI NORMALE
set.seed(123)
LGN(taille = 1000, rloi = rnorm, moyenne = 0, titre = "Loi normale")

# LOI EXPONENTIELLE
set.seed(123)
LGN(taille = 1000, rloi = rexp, moyenne = 1, titre = "Loi exponentielle")

# LOI DE CAUCHY
set.seed(123)
LGN(taille = 1000, rloi = rcauchy, titre = "Loi de Cauchy")

#--- THEOREME DE LA LIMITE CENTRALE ---#

TCL <- function(taille, loi, nrep = 1000, titre = "", ...) {
  # Taille de l'échantillon
  n <- taille
  # Simulation de n x nrep variables aléatoires de loi donnée
  # On simule nrep fois un n-échantillon
  # les ... permettent de spécifier des paramètres supplémentaires 
  X <- matrix(data = loi(n = n*nrep, ...), ncol = nrep)
  # On calcule la meoyenne par colonne
  m.est <- colMeans(x = X)
  # On centre et on réduit avec les estimateurs (moyenne et écart-type empiriques)
  # Pas besoin de connaître la loi
  # Attention : du coup le racine carré de n est inclus dans l'écart-type !
  m.est <- (m.est - mean(m.est))/sd(m.est)
  # Représentation graphique
  m.est <- as.data.frame(m.est)
  ggplot(m.est, aes(x = m.est)) + 
    geom_histogram(aes(y=..density..), bins = nclass.FD(m.est$m.est)) +
    stat_function(fun = dnorm, col = "red") +
    xlab("") +
    ylab("") +
    labs(title = titre)
}

# LOI NORMALE
set.seed(123)
TCL(taille = 100, loi = rnorm, titre = "Loi normale")

# LOI DE GAMMA
TCL(taille = 100, loi = rgamma, shape = 1, scale = 2, titre = "Loi gamma")

# LOI DE STUDENT
TCL(taille = 100, loi = rt, df = 2.5, titre = "Loi de Student")

# LOI DE STUDENT
TCL(taille = 100, loi = rt, df = 1.5, titre = "Loi de Student")

# LOI DE CAUCHY
TCL(taille = 100, loi = rcauchy, titre = "Loi de Cauchy")

##############
# EXERCICE 2 #
##############

# Constante
kappa <- 0.4785

# Taille de l'échantillon
n <- 100

# Nombre de répétitions
m <- 1000

#--- LOI UNIFORME ---#

# Valeurs pour la loi uniforme
sigma <- sqrt(1/6)
rho <- 1/4

# Simulation
X <- runif(n = n*m, min = -1, max = 1) 
X <- matrix(data = X, nrow = n, ncol = m)
Y <- sqrt(n)*colMeans(X)/sigma

# Fonction de répartition empirique
Fn <- ecdf(Y)
x <- seq(from = -3, by = 0.001, to = 3)
LowerBound <- pnorm(x) - (kappa*rho)/(sqrt(n)*sigma^3)
UpperBound <- pnorm(x) + (kappa*rho)/(sqrt(n)*sigma^3)
Bound <- data.frame(x, LowerBound, UpperBound)

# Représentation graphique et bornes de Berry-Essen  
Y <- as.data.frame(Y)
ggplot(Y, aes(Y)) + 
  stat_ecdf(geom = "step") +
  geom_line(data = Bound, aes(x = x, y = LowerBound), col = "red") +
  geom_line(data = Bound, aes(x = x, y = UpperBound), col = "red") +
  xlab("") +
  ylab("") +
  labs(title = "Loi uniforme")

#--- LOI NORMALE ---#

# Valeurs pour la loi uniforme
sigma <- 1
rho <- 2^(3/2)/sqrt(pi)

# Simulation
X <- rnorm(n = n*m, mean = 0, sd = sigma) 
X <- matrix(data = X, nrow = n, ncol = m)
Y <- sqrt(n)*colMeans(X)/sigma

# Fonction de répartition empirique
Fn <- ecdf(Y)
x <- seq(from = -3, by = 0.001, to = 3)
LowerBound <- pnorm(x) - (kappa*rho)/(sqrt(n)*sigma^3)
UpperBound <- pnorm(x) + (kappa*rho)/(sqrt(n)*sigma^3)
Bound <- data.frame(x, LowerBound, UpperBound)

# Représentation graphique et bornes de Berry-Essen  
Y <- as.data.frame(Y)
ggplot(Y, aes(Y)) + 
  stat_ecdf(geom = "step") +
  geom_line(data = Bound, aes(x = x, y = LowerBound), col = "red") +
  geom_line(data = Bound, aes(x = x, y = UpperBound), col = "red") +
  xlab("") +
  ylab("") +
  labs(title = "Loi normale")

#--- LOI NORMALE ---#
# avec plusieurs fonctions de répartition empirique

nb.sim <- 20

# Valeurs pour la loi uniforme
sigma <- 1
rho <- 2^(3/2)/sqrt(pi)

# Simulation
X <- rnorm(n = n*m, mean = 0, sd = sigma) 
X <- matrix(data = X, nrow = n, ncol = m)
Y <- sqrt(n)*colMeans(X)/sigma

# Fonction de répartition empirique
Fn <- ecdf(Y)
x <- seq(from = -3, by = 0.001, to = 3)
LowerBound <- pnorm(x) - (kappa*rho)/(sqrt(n)*sigma^3)
UpperBound <- pnorm(x) + (kappa*rho)/(sqrt(n)*sigma^3)
Bound <- data.frame(x, LowerBound, UpperBound)

# Représentation graphique et bornes de Berry-Essen  
Y <- as.data.frame(Y)
Graphique <- ggplot(Y, aes(Y)) + 
  stat_ecdf(geom = "step") +
  geom_line(data = Bound, aes(x = x, y = LowerBound), col = "red") +
  geom_line(data = Bound, aes(x = x, y = UpperBound), col = "red") +
  xlab("") +
  ylab("") +
  labs(title = "Loi normale")

for (i in 1:(nb.sim-1)) {
  X <- rnorm(n = n*m, mean = 0, sd = sigma) 
  X <- matrix(data = X, nrow = n, ncol = m)
  Y <- sqrt(n)*colMeans(X)/sigma
  Y <- as.data.frame(Y)
  Graphique <- Graphique + 
    stat_ecdf(data = Y, aes(Y), geom = "step", col = gray(level = 1-i/nb.sim))
}
Graphique
