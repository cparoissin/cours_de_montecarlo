#########################
# PREPARATION DU SCRIPT #
#########################

# On efface tous les objets
rm(list = ls())

# On charge les packages
library(ggplot2)

##############
# EXERCICE 1 #
##############

#--- LOI DES GRANDS NOMBRES ---#

LGN <- function(taille, rloi, moyenne = NA, titre = "", jstar = 100) {
  # Taille de l'échantillon
  n <- taille
  # Simulation des variables aléatoires selon la loi donnée en entrée de la fonction
  X <- rloi(n = n)
  # Calcule des moyennes empiriques basées sur le i premières observations
  Xbar <- vector(mode = "numeric", length = n)
  for (i in 1:n) {
    Xbar[i] <- sum(X[1:i])/i
  }  
  Xbar <- Xbar[jstar:n]
  # Représentation graphique
  Xbar <- as.data.frame(Xbar)  
  Graphique <- ggplot(Xbar, aes(x = jstar:n, y = Xbar)) +
    geom_line() +
    xlab("Taille de l'échantillon") +
    ylab("Moyenne empirique")
  if (!is.na(moyenne)) {
    Graphique <- Graphique + geom_hline(yintercept = moyenne, col = "red")
  }
  Graphique +
    labs(title = titre)
}

# LOI NORMALE
set.seed(123)
LGN(taille = 1000, rloi = rnorm, moyenne = 0, titre = "Loi normale")

# LOI EXPONENTIELLE
set.seed(123)
LGN(taille = 1000, rloi = rexp, moyenne = 1, titre = "Loi exponentielle")

# LOI DE CAUCHY
set.seed(123)
LGN(taille = 1000, rloi = rcauchy, titre = "Loi de Cauchy")

#--- THEOREME DE LA LIMITE CENTRALE ---#

TCL <- function(taille, loi, nrep = 1000, titre = "", ...) {
  # Taille de l'échantillon
  n <- taille
  # Simulation des variables aléatoires selon la loi donnée en entrée de la fonction
  # Répétée nrep fois pour avoir nrep statistiques et construire la fonction de répartition empirique
  m.est <- vector(mode = "numeric", length = nrep)
  for (j in 1:nrep) {
    X <- loi(n = n, ...)
    m.est[j] <- mean(X)
  }
  # Centrage et réduction empirique
  m.est <- (m.est - mean(m.est))/sd(m.est)
  # Représentation graphique
  m.est <- as.data.frame(m.est)
  ggplot(m.est, aes(x = m.est)) + 
    geom_histogram(aes(y=..density..), bins = nclass.FD(m.est$m.est)) +
    stat_function(fun = dnorm, col = "red") +
    xlab("") +
    ylab("") +
    labs(title = titre)
}

# LOI NORMALE
set.seed(123)
TCL(taille = 100, loi = rnorm, titre = "Loi normale")

# LOI DE GAMMA
TCL(taille = 100, loi = rgamma, shape = 1, scale = 2, titre = "Loi gamma")

# LOI DE STUDENT
TCL(taille = 100, loi = rt, df = 2.5, titre = "Loi de Student")

# LOI DE STUDENT
TCL(taille = 100, loi = rt, df = 1.5, titre = "Loi de Student")

# LOI DE CAUCHY
TCL(taille = 100, loi = rcauchy, titre = "Loi de Cauchy")

##############
# EXERCICE 2 #
##############

# Constante
kappa <- 0.4785

# Taille de l'échantillon
n <- 100

# Nombre de répétitions
nrep <- 1000

#--- LOI UNIFORME ---#

# Valeurs pour la loi uniforme
sigma <- sqrt(1/6)
rho <- 1/4

# Simulation
# Simulation des variables aléatoires selon la loi donnée en entrée de la fonction
# Répétée nrep fois pour avoir nrep statistiques et construire la fonction de répartition empirique
Y <- vector(mode = "numeric", length = nrep)
for (j in 1:nrep) {
  X <- runif(n = n, min = -1, max = 1) 
  Y[j] <- sqrt(n)*mean(X)/sigma
}

# Fonction de répartition empirique
Fn <- ecdf(Y)
x <- seq(from = -3, by = 0.001, to = 3)
# Bornes de Berry-Esseen
LowerBound <- pnorm(x) - (kappa*rho)/(sqrt(n)*sigma^3)
UpperBound <- pnorm(x) + (kappa*rho)/(sqrt(n)*sigma^3)
Bound <- data.frame(x, LowerBound, UpperBound)

# Représentation graphique et bornes de Berry-Essen  
Y <- as.data.frame(Y)
ggplot(Y, aes(Y)) + 
  stat_ecdf(geom = "step") +
  geom_line(data = Bound, aes(x = x, y = LowerBound), col = "red") +
  geom_line(data = Bound, aes(x = x, y = UpperBound), col = "red") +
  xlab("") +
  ylab("") +
  labs(title = "Loi uniforme")

#--- LOI NORMALE ---#

# Valeurs pour la loi uniforme
sigma <- 1
rho <- 2^(3/2)/sqrt(pi)


# Simulation
# Simulation des variables aléatoires selon la loi donnée en entrée de la fonction
# Répétée nrep fois pour avoir nrep statistiques et construire la fonction de répartition empirique
Y <- vector(mode = "numeric", length = nrep)
for (j in 1:nrep) {
  X <- rnorm(n = n, mean = 0, sd = sigma)  
  Y[j] <- sqrt(n)*mean(X)/sigma
}

# Fonction de répartition empirique
Fn <- ecdf(Y)
x <- seq(from = -3, by = 0.001, to = 3)
# Bornes de Berry-Esseen
LowerBound <- pnorm(x) - (kappa*rho)/(sqrt(n)*sigma^3)
UpperBound <- pnorm(x) + (kappa*rho)/(sqrt(n)*sigma^3)
Bound <- data.frame(x, LowerBound, UpperBound)

# Représentation graphique et bornes de Berry-Essen  
Y <- as.data.frame(Y)
ggplot(Y, aes(Y)) + 
  stat_ecdf(geom = "step") +
  geom_line(data = Bound, aes(x = x, y = LowerBound), col = "red") +
  geom_line(data = Bound, aes(x = x, y = UpperBound), col = "red") +
  xlab("") +
  ylab("") +
  labs(title = "Loi normale")


