#########################
# PREPARATION DU SCRIPT #
#########################

# On efface tous les objets
rm(list = ls())

# On charge les packages
library(ggplot2)

#####################
# EXERCICE 1        #
# LOI DE BERNOUILLI #
#####################

p <- 0.62

bernoulli <- function(n = 1, p = 0.5) {
  u <- runif(n,0,1)
  x <- as.integer(u > 1-p)
  return(x)
}

n <- 1000
X <- bernoulli(n, p)

prob.emp <- prop.table(table(X))
prob.theo <- c(1-p,p)

# Diagramme en barres avec la fonction de base
barplot(rbind(prob.emp,prob.theo), beside = TRUE)

# Diagramme en barres avec le package ggplot2
prob <- data.frame(
  Valeurs = c("0","1","0","1"),
  Cas = c("théorique", "théorique", "empirique", "empirique"),
  Probabilités = c(prob.theo, prob.emp)
)
ggplot(data = prob, aes(x = Valeurs, y = Probabilités, fill = Cas)) +
  geom_bar(stat="identity", position=position_dodge())


#####################
# EXERCICE 2        #
# LOI EXPONENTIELLE #
#####################

exponentielle <- function(n = 1, lambda = 1) {
  u <- runif(n,0,1)
  x <- -log(1-u)/lambda
  return(x)
}

lambda <- 1
n <- 1000
X <- exponentielle(n,lambda)

# Histogramme avec la fonction de base
hist(X, probability = TRUE)
x <- seq(from = 0, by = 0.01, to = 1.2*max(X))
#y <- dexp(x, rate = lambda)
y <- lambda*exp(-lambda*x)
lines(x, y, col = "red", lwd = 2)
lines(density(X), col = "blue", lwd = 2)

# Histogramme avec le package ggplot2
X <- as.data.frame(X)
ggplot(X, aes(x=X)) + 
  geom_histogram(aes(y=..density..)) +
  geom_density(col = "blue") + 
  stat_function(fun = dexp, args = list(rate = lambda), col = "red")

###################
# EXERCICE 3      #
# LOI GEOMETRIQUE #
###################

set.seed(123)

# À partir de la définition de la loi

geometrique.1 <- function(p = 0.5, eps = 0.001){
  n.max <- ceiling(log(eps)/log(1-p))
  U <- bernoulli(n.max, p)
  idx <- which(U==1)
  
  while(length(idx)==0) {
    U <- bernoulli(n.max, p)
    idx <- which(U==1)
  }
  X <- idx[1]
  return(X)
}

X <- geometrique.1()

X <- replicate(n = 100000, expr = geometrique.1())

Xmax <- max(X) 
X <- factor(X, level = 1:Xmax, order = TRUE)
prob.emp <- prop.table(table(X))
prob.theo <- (1-p)^(0:(Xmax-1))*p 

# Diagramme en barres avec la fonction de base
barplot(rbind(prob.emp,prob.theo), beside = TRUE)

# Diagramme en barres avec le package ggplot2
prob <- data.frame(
  Valeurs = rep(x = factor(1:Xmax, ordered = TRUE), times = 2),
  Cas = rep(x = c("théorique", "empirique"), each = Xmax),
  Probabilités = c(prob.theo, prob.emp)
)
ggplot(data = prob, aes(x = Valeurs, y = Probabilités, fill = Cas)) +
  geom_bar(stat="identity", position=position_dodge())

# À partir du lien avec la loi exponentielle

geometrique.2 <- function(n = 1, p = 0.5){
 lambda <- -log(1-p)
 Y <- exponentielle(n,lambda)
 X <- floor(Y) + 1
 return(X)
}

X <- geometrique.2(n = 10000)

Xmax <- max(X) 
X <- factor(X, level = 1:Xmax, order = TRUE)
prob.emp <- prop.table(table(X))
prob.theo <- (1-p)^(0:(Xmax-1))*p 

# Diagramme en barres avec la fonction de base
barplot(rbind(prob.emp,prob.theo), beside = TRUE)

# Diagramme en barres avec le package ggplot2
prob <- data.frame(
  Valeurs = rep(x = factor(1:Xmax, ordered = TRUE), times = 2),
  Cas = rep(x = c("théorique", "empirique"), each = Xmax),
  Probabilités = c(prob.theo, prob.emp)
)
ggplot(data = prob, aes(x = Valeurs, y = Probabilités, fill = Cas)) +
  geom_bar(stat="identity", position=position_dodge())



############################
# EXERCICE 4               #
# LOI DOUBLE EXPONENTIELLE #
############################

double.exponentielle <- function(n = 1, lambda = 1) {
  v <- bernoulli(n = n, p = .5)
  u <- 2*v - 1
  y <- exponentielle(n = n, lambda = lambda)
  x <- u*y
  return(x)
}

lambda <- 1
n <- 1000
X <- double.exponentielle(n = n, lambda = lambda)

# Histogramme avec la fonction de base
xmax <- max(abs(X))
x <- seq(from = -xmax, by = 0.01, to = 1.2*xmax)
y <- lambda/2*exp(-lambda*abs(x))
plot(x, y, col = "red", lwd = 2, type = "l")
lines(density(X), col = "blue", lwd = 2)
hist(X, probability = TRUE, add = TRUE)

# Histogramme avec le package ggplot2
ddexp <- function(x, lambda = 1) {
 res <- dexp(x = abs(x), rate = lambda)/2
 return(res)
}
X <- as.data.frame(X)
ggplot(X, aes(x=X)) + 
  geom_histogram(aes(y=..density..)) +
  geom_density(col = "blue") + 
  stat_function(fun = ddexp, args = list(lambda = lambda), col = "red")



###############
# EXERCICE 5  #
# LOI NORMALE #
###############

# Utilisation du TCL

normale.1 <- function(n = 1, p = 12) {
 u <- runif(n = n*p, min = -0.5, max = 0.5) 
 u <- matrix(u, nrow = n, ncol = p)
 x <- rowSums(u)
 return(x)
}

n <- 1000
X <- normale.1(n = n)

# Histogramme avec la fonction de base
x <- seq(from = -3, by = 0.01, to = 3)
y <- dnorm(x)
plot(x, y, col = "red", lwd = 2, type = "l")
lines(density(X), col = "blue", lwd = 2)
hist(X, probability = TRUE, add = TRUE)

# Histogramme avec le package ggplot2
X <- as.data.frame(X)
ggplot(X, aes(x=X)) + 
  geom_histogram(aes(y=..density..)) +
  geom_density(col = "blue") + 
  stat_function(fun = dnorm)


# Utilisation de la méthode d'inversion avec approximation de la fonction quantile

normale.2 <- function(n = 1) {
 a0 <- 2.30753
 a1 <- 0.27061
 b1 <- 0.99229
 b2 <- 0.04481
 u <- runif(n = n, min = 0, max = 0.5) 
 t <- sqrt(-2*log(u))
 y <- t - (a0 + a1*t)/(1 + b1*t + b2*t^2) 
 s <- bernoulli(n = n, p = .5)
 s <- 2*s - 1
 x <-s*y
 return(x)
}

n <- 1000
X <- normale.2(n = n)

# Histogramme avec la fonction de base
x <- seq(from = -3, by = 0.01, to = 3)
y <- dnorm(x)
plot(x, y, col = "red", lwd = 2, type = "l")
lines(density(X), col = "blue", lwd = 2)
hist(X, probability = TRUE, add = TRUE)

# Histogramme avec le package ggplot2
X <- as.data.frame(X)
ggplot(X, aes(x=X)) + 
  geom_histogram(aes(y=..density..)) +
  geom_density(col = "blue") + 
  stat_function(fun = dnorm)

# Méthode de Box-Muller

normale.3 <- function(n = 1) {
 u <- runif(n = n)
 v <- runif(n = n)
 r <- sqrt(-2*log(u))
 t <- cos(2*pi*v)
 x <- r*t  
 return(x)
}

n <- 1000
X <- normale.3(n = n)

# Histogramme avec la fonction de base
x <- seq(from = -3, by = 0.01, to = 3)
y <- dnorm(x)
plot(x, y, col = "red", lwd = 2, type = "l")
lines(density(X), col = "blue", lwd = 2)
hist(X, probability = TRUE, add = TRUE)

# Histogramme avec le package ggplot2
X <- as.data.frame(X)
ggplot(X, aes(x=X)) + 
  geom_histogram(aes(y=..density..)) +
  geom_density(col = "blue") + 
  stat_function(fun = dnorm)

#################
# EXERCICE 6    #
# LOI DE CAUCHY #
#################

# Méthode 1

cauchy.1 <- function(n = 1) {
  z1 <- rnorm(n = n)
  z2 <- rnorm(n = n)
  x <- z1/z2
  return(x)
}

n <- 1000
X <- cauchy.1(n = n)

# Représentation graphique... compliquée à faire

# Avec toutes les observations : on ne voit rien...

xmax <- max(abs(X))
x <- seq(from = -xmax, by = 0.01, to = 1.2*xmax)
#y <- dexp(x, rate = lambda)
y <- dcauchy(x = x)
plot(x, y, col = "red", lwd = 2, type = "l")
lines(density(X), col = "blue", lwd = 2)
hist(X, probability = TRUE, add = TRUE)

# En zoomant : on ne voit rien non plus...

xmax <- 10
x <- seq(from = -xmax, by = 0.01, to = 1.2*xmax)
#y <- dexp(x, rate = lambda)
y <- dcauchy(x = x)
plot(x, y, col = "red", lwd = 2, type = "l")
lines(density(X), col = "blue", lwd = 2)
hist(X, probability = TRUE, add = TRUE)

# En ne retenant que les valeurs centrales : on s'en sort !

xmax <- 10
x <- seq(from = -xmax, by = 0.01, to = 1.2*xmax)
#y <- dexp(x, rate = lambda)
y <- dcauchy(x = x)
plot(x, y, col = "red", lwd = 2, type = "l")
X.subset <- X[which(abs(X) <= xmax)]
lines(density(X.subset), col = "blue", lwd = 2)
hist(X.subset, probability = TRUE, add = TRUE)

# Méthode 2

cauchy.2 <- function(n = 1) {
 x <- vector(mode = "numeric", length = n)
 for (i in 1:n) {
  z1 <- runif(n = 1, min = -1/4, max = 1/4)
  z2 <- runif(n = 1, min = -1/4, max = 1/4)
  while (sqrt(z1^2 + z2^2) > 1/4) {
   z1 <- runif(n = 1, min = -1/4, max = 1/4)
   z2 <- runif(n = 1, min = -1/4, max = 1/4)
  }  
  x[i] <- z1/z2
 }
 return(x)
}

n <- 1000
X <- cauchy.2(n = n)

# En ne retenant que les valeurs centrales : on s'en sort !

xmax <- 10
x <- seq(from = -xmax, by = 0.01, to = 1.2*xmax)
#y <- dexp(x, rate = lambda)
y <- dcauchy(x = x)
plot(x, y, col = "red", lwd = 2, type = "l")
X.subset <- X[which(abs(X) <= xmax)]
lines(density(X.subset), col = "blue", lwd = 2)
hist(X.subset, probability = TRUE, add = TRUE)














