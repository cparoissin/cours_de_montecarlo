# Définition de la loi de proba à simuler à travers le rapport de probas
# Ex 1 : loi de Katz
RatioProb <- function(x, alpha, beta) {
  res <- ifelse(alpha + beta *x < 0, 0, (alpha + beta *x)/(1 + x))
  return(res)
}

# Matrice P
P <- function(i, j, alpha, beta) {
 res <- 0
 if (j == i+1) {
   res <- 1/2*min(1, RatioProb(x = i, alpha, beta))
 }
 if ((i > 0) & (j == i-1)) {
   res <- 1/2*min(1, 1/RatioProb(x = i-1, alpha, beta))
 }
 if ((i > 0) & (j == i)) {
   res <- 1 - 1/2*min(1, RatioProb(x = i, alpha, beta)) - 1/2*min(1, 1/RatioProb(x = i-1, alpha, beta))
 }
 if ((i == 0) & (j == 0)) {
   res <- 1 - 1/2*min(1, RatioProb(x = 0, alpha, beta))
 }
 return(res)
}

# Algorithme de Metropolis-Hastings
MH.algo <- function(x.init = 0, nb.iter = 1000, alpha, beta) {
  i <- 1
  x <- x.init
  while (i <= nb.iter) {
    if (x == 0) {
      x <- sample(x = c(0,1), size = 1, prob = c(P(0, 0, alpha, beta),P(0, 1, alpha, beta)))
    } else {
      x <- sample(x = c(x-1,x,x+1), size = 1, prob = c(P(x,x-1,alpha, beta),P(x,x,alpha, beta),P(x,x+1,alpha, beta)))
    }
    i <- i+1
  }
  return(x)
}

##########
# TEST 1 #
##########

alpha <- 1
beta <- 1

# Simulation d'une variable aléatoire
x <- MH.algo(alpha = alpha, beta = beta)

# Simulation de plusieurs variables aléatoires
n <- 100
X <- replicate(n = n, expr = MH.algo(alpha = alpha, beta = beta))

##################
# TEST 2         #
# LOI DE POISSON #
##################

lambda <- 2
n <- 1000

X <- replicate(n = n, expr = MH.algo(alpha = lambda, beta = 0))

Xmax <- max(X) 
X <- factor(X, level = 0:Xmax, order = TRUE)
prob.emp <- prop.table(table(X))
prob.theo <- dpois(x = 0:Xmax, lambda = lambda)

# Diagramme en barres avec le package ggplot2
library(ggplot2)
prob <- data.frame(
  Valeurs = rep(x = factor(0:Xmax, ordered = TRUE), times = 2),
  Cas = rep(x = c("théorique", "empirique"), each = length(prob.emp)),
  Probabilités = c(prob.theo, prob.emp)
)
ggplot(data = prob, aes(x = Valeurs, y = Probabilités, fill = Cas)) +
  geom_bar(stat="identity", position=position_dodge())











