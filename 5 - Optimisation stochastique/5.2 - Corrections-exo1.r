initialise <- function(nv) {
 # Tire les coordonnees de nb villes au hasard dans le carre unite. Calcule la matrice des distances. 
 v <- runif(n = 2*nv)
 v <- matrix(data = v, nrow = nv, ncol = 2)
 distances <- dist(x = v, diag = TRUE, upper = TRUE)
 distances <- as.matrix(distances)
 res <- list(v, distances)
 return(res)
}

# villes <- c(1:nv,1)

trace_parcours <- function(v, circuit) {
 # Represente les villes et le circuit dans le carre unite.
 plot(x = v[,1], y = v[,2], type = "p", pch = 19)
 lines(x = v[circuit,1], y = v[circuit,2])
}

f <- function(ordre) {
 # fonction d'energie : somme des distances du circuit dans l'ordre donne en entree.
 ordre2 <- c(ordre,ordre[1])
 energie <- sum(diag(distances[ordre2[1:nv],ordre2[2:(nv+1)]]))
 return(energie)
}

permuter <- function(ordre, k = 5) {
 # Permute 5 indices consecutifs d'un vecteur d'ordre
 # Indice de depart
 v <- sample(x = 1:nv, size = 1)
 # ordre initial 
 o <- ordre
 if (v>1) {
  # decalage
  o <- o[c(v:nv,1:(v-1))]
 } 
 o2 <- sample(x = 1:k, size = k)
 # ordre modifie 
 o[1:k] <- o[o2]
 return(o)
}

recuit <- function(ordre, h, unsurT, distances) {
#     Simule nbre pas de l'algorithme de recuit simule
#     pour le probleme du voyageur de commerce.
#     Retourne le nouvel ordre et le vecteur des valeurs
#     successives de la fonction f.
 f1 <- f(ordre)
 energie <- f1 
 ordre.opt <- ordre
 n <- 0
 for (ut in 1:unsurT) {
  palier = ceiling(exp(ut*h))
  while (n < palier) {
   ordre2 <- permuter(ordre)
   f2 <- f(ordre2)
   if (f2 < f1) {
    ordre <- ordre2
    f1 <- f2
   } else {
    p <- exp((f1-f2)*ut)
    if (runif(1) < p) {
     ordre <- ordre2
     f1 <- f2
    }
   }
   if (f1 < min(energie)) {
    ordre.opt <- ordre
   }
   energie <- c(energie, f1)
   n <- n+1
  }
 }
 res <- list(ordre.opt, energie)
 return(res)
}


#################

# Nombre de villes
set.seed(123)
nv <- 20
init <- initialise(nv)
v <- init[[1]]
distances <- init[[2]] 
ordre <- 1:nv
circuit <- c(ordre,ordre[1])
# circuit initial
# trace_parcours(v, circuit) 

# Paramètres
h <- 0.6
u <- 17

#################

# algorithme du recuit simulé
opt <- recuit(ordre, h, u, distances)   
circuit.opt <- c(opt[[1]],opt[[1]][1])
energie <- opt[[2]]
# circuit final
trace_parcours(v, circuit.opt) 
# évolution de l'énergie
# plot(energie, type = "l", lwd = 2, col = "red", xlab = "Itération", ylab = "Energie")










