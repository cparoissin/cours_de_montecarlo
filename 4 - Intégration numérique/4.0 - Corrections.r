##########################
# INTEGRATION NUMERIQUE  #
# METHODES DETERMINISTES #
##########################

#--- METHODE DES TRAPEZES ###

Trapeze <- function(f = FUN, a, b, n) {
  h <- (b-a)/n
  x <- seq(from = a, by = h, to = b)
  res <- (f(x[1]) + f(x[n+1]) + 2 * sum(f(x[2:n])))*h/2
  return(res)
}

f1 <- function(x) {
  1/(1+x)
}

Trapeze(f = f1, a = 0, b = 1, n = 10)
Trapeze(f = f1, a = 0, b = 1, n = 100)
Trapeze(f = f1, a = 0, b = 1, n = 1000)

f2 <- function(x) {
  if (x == 0) {
    res <- 1
  } else {
    res <- x/(exp(x)-1)
  }
  return(res)
}

Trapeze(f = f2, a = 0, b = 1, n = 10)

f2 <- function(x) {
  ifelse(x == 0, 1, x/(exp(x)-1))
}

Trapeze(f = f2, a = 0, b = 1, n = 10)
Trapeze(f = f2, a = 0, b = 1, n = 100)
Trapeze(f = f2, a = 0, b = 1, n = 1000)


f3 <- function(x) {
  x^(3/2)
}

Trapeze(f = f3, a = 0, b = 1, n = 10)
Trapeze(f = f3, a = 0, b = 1, n = 100)
Trapeze(f = f3, a = 0, b = 1, n = 1000)
Trapeze(f = f3, a = 0, b = 1, n = 10000)


f4 <- function(x) {
  x^(1/2)
}

Trapeze(f = f4, a = 0, b = 1, n = 10)
Trapeze(f = f4, a = 0, b = 1, n = 100)
Trapeze(f = f4, a = 0, b = 1, n = 1000)
Trapeze(f = f4, a = 0, b = 1, n = 10000)

#--- METHODE DE SIMPSON ###

Simpson <- function(f = FUN, a, b, n) {
  h <- (b-a)/(2*n)
  x <- seq(from = a, by = h, to = b)
  i <- 0:(n-1)
  i1 <- 2*i
  i2 <- 2*i+1
  i3 <- 2*i+2
  res <- (sum(f(x[i1])) + 4 * sum(f(x[i2])) + sum(f(x[i3])))*h/3
  return(res)
}

f1 <- function(x) {
  1/(1+x)
}

Simpson(f = f1, a = 0, b = 1, n = 10)


